from flask import Flask
from flask import request


# creates a Flask application, named app
app = Flask(__name__)

import ner_en

@app.route("/", methods=['GET'])
def display_bot_message():
    user_input = request.args.get('msg')

    if user_input is not None:
        return ner_en.response(user_input)
    return ner_en.welcome_msg()


# run the application
if __name__ == "__main__":
    app.run(debug=True)