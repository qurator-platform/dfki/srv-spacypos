# Spacy Pos-Tagger (english only)
author: Karolina Zaczynska  
date: August 2019  
description: A part-of-speech-tagger for english texts, tagset: Universal POS tags
