import spacy
from collections import OrderedDict
import json

def response(user_input):
    nlp = spacy.load('en_core_web_sm')
    doc = nlp(user_input)
    raw_text = []
    pos_text = []
    new_list=[]
    for token in doc:

        raw_text.append(token.text)
        pos_text.append(token.pos_)
        json_dump = json.dumps([{'token' : token, 'pos' : pos} for token, pos in zip(raw_text, pos_text)])

    return json_dump


def welcome_msg():
    '''
    Returns a welcome/info text about the service.
    '''
    msg = 'This is a simple spacy pos tagger. Add to URL: "/?msg=INPUT_TEXT"'
    return msg




