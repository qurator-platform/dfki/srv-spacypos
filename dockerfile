from python:3

COPY ner_en.py .
COPY flask-api.py .
COPY requirements.txt .

RUN pip install -r requirements.txt
RUN python -m spacy download en_core_web_sm

EXPOSE 8080
ENTRYPOINT FLASK_APP=flask-api.py flask run --host=0.0.0.0 --port=8080

